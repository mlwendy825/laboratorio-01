/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_01;


import java.util.Scanner;

/**
 *
 * @author W.Moreno
 */
public class LAB_01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        programa(args);
    }

    public static void programa(String[] args) {// inicio programa

        Scanner opmenu = new Scanner(System.in);

        int op1;
        System.out.print("*  Bienvenido al Menú de Opciones  *\n");
        System.out.print("*                                  *\n");
        System.out.print("*               1. PPT             *\n");
        System.out.print("*               2. Cine            *\n");
        System.out.print("*               3. Sets            *\n");
        System.out.print("*               4. Salir           *\n");
        System.out.print("************************************\n");
        System.out.print("Digite el numero de la opcion: ");
        op1 = opmenu.nextInt();
        
        

        System.out.print("\n");

        //Mientras la opción elegida sea 0, preguntamos al usuari
        
        //while (op2 !=5) {// inicio del while

            //Try catch para evitar que el programa termine si hay un error
            try {// inicio del try

                switch (op1) {// inicio switch
                    case 1:
                             System.out.println("Has seleccionado el Juego de Piedra, Papel o Tijera");
                             System.out.println("              ");
                        
                       
                             boolean continuar = true;
                             
                             while (continuar == true)
                             {
                                  Scanner opPPT = new Scanner(System.in);
                        
                                  int op;
                        
                                System.out.println(" Menú de Opciones  * ");
                                System.out.println(" 0 = Pierdra       *");
                                System.out.println(" 1 = Papel         *");
                                System.out.println(" 2 = Tijera        *");
                                System.out.print("********************\n");
                                System.out.print("Digite el numero de la opcion: ");
                                op = opPPT.nextInt();
                                
                                int movi = op;
                          
                                //("Genera al azar piedra, papel o tijera:")//
                                int mano = (int)(Math.random()*3); // genera un número al azar // entre 1 y 3 ambos incluidos
                                System.out.print("Opcion del Computador: "+ mano);
                                System.out.println("\n");
                                
                                
                                if (op == mano)
                                {
                                     System.out.print("********************\n");
                                    System.out.println("Hay un empate\n");
                                            
                                }
                                
                                else if (op == 2  && mano == 1)
                                    
                                {
                                    System.out.println("Usuario = Tijera");
                                    System.out.println("Computador = Papel\n");
                                     System.out.print("********************\n");
                                    System.out.println("Felicidades!");
                                    System.out.println("HAS GANADO\n");
                                   
                                    
                                }
                                
                                 else if (op == 2  && mano == 0)
                                {
                                     System.out.println("Usuario = Tijera");
                                    System.out.println("Computador = Piedra\n");
                                     System.out.print("********************\n");
                                     System.out.println("Felicidades!");
                                    System.out.println("HAS GANADO\n");
                                    
                                }
                               else if (mano == 2  && op == 0)
                                   
                                {
                                      System.out.println("Usuario = Piedra");
                                    System.out.println("Computador = Tijera\n");
                                     System.out.print("********************\n");
                                     System.out.println("ups!");
                                    System.out.println("HAS PERDIDO\n");
                                    
                                }
                                
                                else if (mano == 2  && op == 1)
                                {
                                     System.out.println("Usuario = Papel");
                                    System.out.println("Computador = Tijera\n");
                                     System.out.print("********************\n");
                                     System.out.println("ups!");
                                    System.out.println("HAS PERDIDO\n");
                                    
                                }
                                
                                
                                else if (op == 1  && mano == 0)
                                    
                                {
                                    System.out.println("Usuario = Papel");
                                    System.out.println("Computador = Piedra\n");
                                     System.out.print("********************\n");
                                    System.out.println("Felicidades!");
                                    System.out.println("HAS GANADO\n");
                                   
                                    
                                }
                                
                                 else if (op == 0  && mano == 1)
                                {
                                     System.out.println("Usuario = Piedra");
                                    System.out.println("Computador = Papel\n");
                                     System.out.print("********************\n");
                                     System.out.println("ups!");
                                    System.out.println("HAS PERDIDO\n");
                                    
                                    
                                }
                                
                                Scanner con = new Scanner(System.in);
                                int respuesta;
                                System.out.println("Quieres seguir jugando?");
                                System.out.println("PRESIONE  (1) PARA CONTINUAR ");
                                System.out.println("PRESIONE  (2) PARA SALIR");
                                respuesta = con.nextInt();
                              
                                if ( respuesta == 1)
                                {
                                   continuar = true;
                                }
                                   else  {
                                           continuar = false;
                                           }
                                
                             }
                       
                        break;
                         // FIN PPT
                        
                        // INICIO CINE

                    case 2:

                        System.out.println("Has seleccionado La Sala de Cine");
                        System.out.println("\n");
                        Scanner opcine = new Scanner(System.in);
                        int opc;
                        System.out.print("********************\n");
                        System.out.println(" Menú de Opciones ");
                                System.out.println(" 1 = Ver estado de la Sala");
                                System.out.println(" 2 = Elegir asientos");
                                System.out.println(" 3 = Salir");
                                System.out.print("********************\n");
                                System.out.print("Digite el numero de la opcion: ");
                                opc = opcine.nextInt();
                                
                                if (opc == 1)
                                {
                                    String [][] Acientos = 
                                    {
                                        {"8A","8B","8C","8D","8E","8F","8G","8H","8I"},
                                        {"7A","7B","7C","7D","7E","7F","7G","7H","7I"},
                                        {"6A","6B","6C","6D","6E","6F","6G","6H","6I"},
                                        {"5A","5B","5C","5D","5E","5F","5G","5H","5I"},
                                        {"4A","4B","4C","4D","4E","4F","4G","4H","4I"},
                                        {"3A","3B","3C","3D","3E","3F","3G","3H","3I"},
                                        {"2A","2B","2C","2D","2E","2F","2G","2H","2I"},
                                        {"1A","1B","1C","1D","1E","1F","1G","1H","1I"},
                                    };
                                    
                                       //Como correr las matrices
        
                                       for (int x = 0;  x < Acientos.length; x++)
                                      {
                                          for (int y = 0; y < Acientos [x].length; y ++)
                                         {
                                              System.out.println(Acientos [x][y]);
                                              if (y!= Acientos [x].length ) System.out.print("  "  + " ");
                                          }
                                          
                                          
                                          //LO INTENTE :(
                                          
                                     }
                                }
                        break;

                        
                    case 3:
                        System.out.println("Has seleccionado SETS");
                        System.out.println("");
                        
                        //Un partido de tenis se divide en sets. Para ganar un set, un jugador debe
                        //ganar 6 juegos, pero además debe haber ganado por lo menos dos juegos
                        //más que su rival. Si el set está empatado a 5 juegos, el ganador es el primero
                        //que llegue a 7. Si el set está empatado a 6 juegos, el set se define en un
                        //último juego, en cuyo caso el resultado final es 7-6
                        
                       boolean continua = true;
                             
                       while (continua == true)
                      {
                        
                        Scanner juego = new Scanner(System.in);
                        int opjue1;
                        System.out.println("Ingrese los juegos ganados del primer equipo:  ");
                        opjue1 =  juego.nextInt();
                        
                        
                        int opjue2;
                        System.out.println("Ingrese los juegos ganados del segundo equipo:  ");
                        opjue2 =  juego.nextInt();
                        
                                 if (opjue1 <=7 && opjue2 <=7)
                                    {
                                        System.out.println("");
                            
                                        if (opjue1 > opjue2)
                                           {
                                           int resta;
                                
                                            resta = (opjue1 - opjue2);
                                
                                             //System.out.println(""+ resta);
                                             if (resta == 2)
                                             {  
                                                    System.out.println("Primer Jugador: "+ opjue1);
                                                    System.out.println(" Segundo Jugador: " + opjue2 );
                                                    System.out.println("Resultado = GANÓ EL PRIMER JUGADOR\n3"
                                                            + "33");
                                               }
                                 
                                            if (resta == 1)
                                             {
                                                System.out.println("Primer Jugador: "+ opjue1);
                                                System.out.println(" Segundo Jugador: " + opjue2 );
                                                System.out.println("Resultado = EL SET AUN NO TERMINA\n");
                                             }
                                            
                                             else if (resta >= 3)
                                             {
                                                   System.out.println("debe haber ganado por lo menos dos juegos más que su rival  o ");
                                                    System.out.println("debe haber perdido por lo menos dos juegos menos que su rival.\n");
                                              }
                                  
                                          }// fin (opjue1 > opjue2)
                            
                                          if (opjue1 < opjue2)
                                            {
                                            int resta;
                                
                                                  resta = (opjue2 - opjue1);
                                
                                         //System.out.println(""+ resta);
                                         if (resta == 2)
                                             {
                                               System.out.println("Primer Jugador: "+ opjue1);
                                               System.out.println(" Segundo Jugador: " + opjue2 );
                                               System.out.println("Resultado = GANÓ EL SEGUNDO  JUGADOR\n");
                                             }
                                 
                                            if (resta == 1)
                                             {
                                                System.out.println("Primer Jugador: "+ opjue1);
                                                System.out.println(" Segundo Jugador: " + opjue2 );
                                                System.out.println("Resultado = EL SET AUN NO TERMINA\n");
                                              }
                                            else if (resta >= 3)
                                             {
                                                   System.out.println("debe haber ganado por lo menos dos juegos más que su rival.");
                                                    System.out.println("debe haber perdido por lo menos dos juegos menos que su rival.\n");
                                              }
                                  
                                          }// fin (opjue1 < opjue2)
                                     }       
                        
                                   else
                                    {
                                           System.out.println("el set es inválido");
                                     }
                               Scanner con = new Scanner(System.in);
                                int respuesta;
                                System.out.println("Quieres seguir jugando?");
                                System.out.println("PRESIONE  (1) PARA CONTINUAR ");
                                System.out.println("PRESIONE  (2) PARA SALIR");
                                respuesta = con.nextInt();
                              
                                if ( respuesta == 1)
                                {
                                   continua = true;
                                }
                                   else  {
                                           continua = false;
                                           }
                                
                             }
                       
                        break;
                        
                    case 4:

                        System.out.println("Has seleccionado Salir del Programa");

                        System.out.println("Hasta Luego!");
                        System.out.println("Vuelve pronto....");
                        break;
                        

                    default:
                        System.out.println("HAS DIGITADO UN NUMERO DE OPCION INVALIDO");
                        System.out.println("POR FAVOR VERIFICA BIEN LAS OPCIONES.....");
                        
                        break;
                        
                }

            } catch (Exception e) {
                System.out.println("Uoop! Error!");
            } // Fin del Try

       //}// fin del While

    }// fin de programa
}
